from .models import AutomobileVO, Customer, Sale, Salesperson
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
        "import_href",
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "customer",
        "salesperson",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "customer": CustomerDetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def listCustomers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerDetailEncoder,
            safe=False)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False)


@require_http_methods(["GET", "POST"])
def listSalespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonDetailEncoder,
            safe=False)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False)


@require_http_methods(["GET", "POST"])
def listSales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
            safe=False)
    else:
        content = json.loads(request.body)

        try:
            auto_vin = content["automobile"]
            autoVO = AutomobileVO.objects.get(vin=auto_vin)
            if autoVO.sold is False:
                content["automobile"] = autoVO
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Message: Invalid vin number."},
                status=400)
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Message: Invalid Salesperson."},
                status=400)
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Message: Invalid Customer."},
                status=400)

        json_data = json.dumps({"sold": True})
        AutoUrl = (
            f"http://project-beta-inventory-api-1:8000/api/automobiles/{auto_vin}/"
        )
        response = requests.put(
            AutoUrl, data=json_data, headers={"Content-Type": "application/json"}
        )
        if response.status_code != 200:
            print("auto request failed", response.status_code)

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False)


@require_http_methods(["GET", "DELETE"])
def viewSales(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False)
    else:
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
