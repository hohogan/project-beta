from django.urls import path
from .views import listCustomers, listSalespeople, listSales, viewSales


urlpatterns = [
    path('customers/', listCustomers, name="listCustomer"),
    path('sales/', listSales, name="listSales"),
    path('sales/<int:id>/', viewSales, name="viewSales"),
    path('salespeople/', listSalespeople, name="listSalespeople"),
]
