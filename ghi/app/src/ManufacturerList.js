//

function ManufacturerList({manufacturers}) {
    return (
      <div className="my-5 container">
        <h1>Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
            </tr>
          </thead>
            <tbody>
              {manufacturers.map(manufacturers => {
                return (
                  <tr key={manufacturers.id}>
                    <td>{manufacturers.name}</td>
                  </tr>
                )
              })}
            </tbody>
        </table>
      </div>
    )
  }

export default ManufacturerList;
