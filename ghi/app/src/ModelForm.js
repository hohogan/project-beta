import React, { useEffect, useState } from "react";

function ModelForm({ getModels}) {
    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    async function fetchManufacturers() {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchManufacturers();
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data  = {
            name: name,
            picture_url: picture_url,
            manufacturer_id: manufacturer,
        };

        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        };
        const response = await fetch(modelUrl, fetchConfig)
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');

            getModels();
            window.location.href = 'http://localhost:3000/models';
        }
    }

    function handleChangeName(event) {
        const { value } = event.target;
        setName(value);
      }

    function handleChangePictureUrl(event) {
        const {value} = event.target;
        setPictureUrl(value);
    }

    function handleChangeManufacturer(event) {
        const {value} = event.target;
        setManufacturer(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-5 mt-5">
                <h1>Create Vehicle Model</h1><br></br>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="mb-3">
                        <select value={manufacturer} onChange={handleChangeManufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option value="">Choose a manufacturer</option>
                            {manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                            )
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={picture_url} onChange={handleChangePictureUrl} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture url</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>

    );
}

export default ModelForm;
