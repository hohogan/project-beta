import React, { useState } from "react";

function ManufacturerForm({getManufacturers}) {
    const [name, setName] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name: name,
        };

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig)
        if (response.ok) {
            setName('');

            getManufacturers();
            window.location.href = 'http://localhost:3000/manufacturers/';
        }
    }

    function handleChangeName(event) {
        const { value } = event.target;
        setName(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-5 mt-5">
                <h1>Create Manufacturer</h1><br></br>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleChangeName} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Manufacturer Name</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
        );

}

export default ManufacturerForm;
