import React, { useState, useEffect } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [autos, setAutos] = useState([]);

  async function fetchAppointments() {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      if (response.ok) {
        const data = await response.json();
        console.log(data);

        const filteredAppointments = data.appointments.filter(appointment => {
            return appointment.status !== "Canceled" && appointment.status !== "Finished";
        })

        setAppointments(filteredAppointments);
      }
    } catch (error) {
      console.error("Could not fetch appointment:", error);
    }
  }

  async function fetchAutos() {
    const secondResponse = await fetch('http://localhost:8100/api/automobiles/')
    if (secondResponse.ok) {
      const data = await secondResponse.json()
      setAutos(data.autos)
    }
  }

  useEffect(() => {
    fetchAppointments();
    fetchAutos();
  }, []);

  async function cancelAppointment(appointmentId){
    const apptURL = (`http://localhost:8080/api/appointments/${appointmentId}/cancel/`)
    const fetchOptions={
    method: "PUT",
    headers: {'Content-Type': 'application/json'}
  }
  const response = await fetch(apptURL, fetchOptions)

  if (response.ok){
    setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== appointmentId));
  }

}

  async function finishAppointment(appointmentId){
    const apptURL = (`http://localhost:8080/api/appointments/${appointmentId}/finish/`)
    const fetchOptions={
    method: "PUT",
    headers: {'Content-Type': 'application/json'}
  }
  const response = await fetch(apptURL, fetchOptions)

  if (response.ok){
    setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== appointmentId));
  }

}

  function formatDate(string) {
    const date = new Date(string)
    const timeString = date.toLocaleTimeString(undefined, { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: '2-digit' })
    return timeString.replace(/\s/g, '').replace(',', ', ')
  }


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date and Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
        // appointments.filter(appt=> filter items).filter(appt=> filter items).map(appt=> render view)
          const auto = autos.find((auto) => auto.vin === appointment.vin);
          const isVIP = auto && auto.sold;
          return (
            <tr key={ appointment.id }>
              <td>{ appointment.vin }</td>
              <td>{ isVIP ? "Yes" : "No" }</td>
              <td>{ appointment.customer }</td>
              <td>{ formatDate(appointment.date_time) }</td>
              <td> { `${appointment.technician.first_name} ${appointment.technician.last_name}` }</td>
              <td>{ appointment.reason }</td>
              <td>
                <button className= "btn btn-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                <button className= "btn btn-success" onClick={() => finishAppointment(appointment.id)}>Finish</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;
