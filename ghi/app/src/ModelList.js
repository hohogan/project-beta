//

function ModelsList({models}) {

    return (
      <div className="my-5 container">
        <h1>Models</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Picture</th>
            </tr>
          </thead>
            <tbody>
              {models.map(model => {
                return (
                  <tr key={model.name}>
                    <td>{model.manufacturer.name}</td>
                    <td>{model.name}</td>
                    <td>
                      <img src={model.picture_url} alt="car model" style={{ width:'150px', height: '100px'}} />
                      </td>
                  </tr>
                )
              })}
            </tbody>
        </table>
      </div>
    )
}

export default ModelsList;
