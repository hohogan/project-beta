from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        try:
            technician = Technician.objects.all()
            return JsonResponse(
                {"technicians": technician},
                encoder=TechnicianListEncoder,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Failed to add technician"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_delete_technician(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(pk=pk)
        return JsonResponse(technicians,
                            encoder=TechnicianListEncoder,
                            safe=False)
    else:
        count, _ = Technician.objects.filter(pk=pk).delete()
        return JsonResponse({"Deleted": count > 0})


def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        appointments_with_vip = []

        automobiles = AutomobileVO.objects.all()

        for appointment in appointments:
            try:
                automobile_vo = automobiles.get(vin=appointment.vin)
                if automobile_vo.sold:
                    appointment.vip = True
                else:
                    appointment.vip = False
            except AutomobileVO.DoesNotExist:
                appointment.vip = False

            appointments_with_vip.append(appointment)

        return JsonResponse(
            {"appointments": appointments_with_vip},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(pk=technician_id)

            content["technician"] = technician
            content["status"] = "created"

            appointment = Appointment(**content)
            appointment.save()
            appointments = Appointment.objects.all()

            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except KeyError:
            response = JsonResponse(
                {"message": "Invalid JSON payload or missing fields"},
                status=400
            )
            return response
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician ID does not exist"},
                status=404
            )
            return response


@require_http_methods(["GET", "POST"])
def api_appointment(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()

        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create appointment"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_delete_appointment(request, pk):
    if request.method == "GET":
        appointments = Appointment.objects.get(pk=pk)
        return JsonResponse(appointments,
                            encoder=AppointmentListEncoder,
                            safe=False)
    else:
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse({'deleted': count > 0})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "Canceled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
                status=200
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Could not cancel appointment"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "Finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
                status=200
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Could not finish appointment"},
                status=400,
            )


# @require_http_methods(["GET"])
# def api_service_history(request):
#     appointments = Appointment.objects.all()
#     return JsonResponse(
#         {"appointments": appointments},
#         encoder=AppointmentListEncoder
#     )
